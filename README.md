# Ansible Playbooks to install CMUPocketSphinx-python

### Goal:

- Prepare a target node for and install [PocketSphinx-python](https://github.com/cmusphinx/pocketsphinx-python)

### Process

1. Resolve all needed dependancies on the remote machine
1. Download each GitHub code base
1. Build each GitHub code base
1. Install SphinxBase
1. Install PocketSphinx
1. Install PocketSphinx-python

### Operation
```
# Populate the group_vars/sphinx-target.yml file. This playbook assumes the target machine is running default vagrant values.
ansible-playbook site.yml
```

### Directory Structure Explanation

See [directory layout](http://docs.ansible.com/ansible/playbooks_best_practices.html#directory-layout) for
Ansible's best practices on laying out your directory structure.

The conf/ directory is a personal preference. All other directories are ansible-defined. 

###### /conf/

Contains the configuration for the playbook which may vary from system to system, company to company.

### Credit
Made by Kurt Bomya
